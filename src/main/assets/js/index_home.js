//That junt needs to happen on load and window resize
$(window).on("resize", function () {
    
    //Measure some responsiveness
    var responsive_viewport = $(window).width();

    // if is larger than 791px
    if (responsive_viewport > 791) {
      
      //Let's make some stuff fullscreen
      $('.billboard, .slide-nav').css('height', window.innerHeight);
      
      //Let's center up that h1 and do some measuering
      jQuery.fn.center = function () {
        this.parent().css("position","absolute");
        var t = this.parent().css("top");
        var l = this.parent().css("left");

        this.css("position","absolute");
        this.css("top", ((this.parent().height() - this.outerHeight()) / 2) + this.parent().scrollTop() + "px");
        this.css("left", ((this.parent().width() - this.outerWidth()) / 2) + this.parent().scrollLeft() + "px");
        return this;
      }

      //Center that junt
      $("h1").center();
      
    }
  
}).resize(); //fire off that resize event cause desingers always be stretching windows and stuff

//Load those nav li's sequrntially for purdyness
window.onload = function() {
  var cars = document.querySelectorAll(".slide-nav ul li"), i = 1;
    Array.prototype.forEach.call(cars, function(car) { 
    setTimeout(function(){ car.classList.add("visible") }, 100*i)
    i++;
  })
};