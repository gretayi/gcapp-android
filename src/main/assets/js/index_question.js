$(document).ready( function() {
  $('nav.menu a').click( function() {
    $(this).parent().find('.current').removeClass('current');
    $(this).addClass('current');
  });
  $('a[data-connect]').click( function() {
    var i = $(this).find('i');
    i.hasClass('icon-collapse-top') ? i.removeClass('icon-collapse-top').addClass('icon-collapse') : i.removeClass('icon-collapse').addClass('icon-collapse-top');
    $(this).parent().parent().toggleClass('all').next().slideToggle();
  });
  $(window).scroll(function(){
    var w = $(window).width();
    if(w < 768) {
      $('#top-button').hide();
    } else {
      var e = $(window).scrollTop();
      e>150?$('#top-button').fadeIn() :$('#top-button').fadeOut();   
    }
  });
  $.ajax({
    type: 'GET', 
    url: 'http://kid37.cc.gatech.edu:8080/gpower/getQuestionList.py', 
    success: function(data) {
      var content = jQuery.parseHTML(data)[0].data;
      data = jQuery.parseJSON(content);
      $.each(data, function(index, element) {
        var icon = $("<div></div>").addClass("large-1 column lpad").append("<i class=\"icon-file\"></i>");
        var text1 = $("<div></div>").addClass("large-1 column lpad").append("<span class=\"center\">9</span>");
        var text2 = $("<div></div>").addClass("large-1 column lpad").append("<span class=\"center\">9</span>");

        var title = $("<span class=\"overflow-control\"></span>").append(
          $("<a></a>").attr("href", "reply.html?question_id="+element.id).text("["+element.category+"]"+element.title)
        );
        var topic = $("<div></div>").addClass("large-7 small-8 column lpad")
          .append(title)
          .append("<span>10-20-2015 7:29PM</span>");

        var reply_number = $("<a></a>").attr("href", "reply.html?question_id="+element.id);
        $.ajax({
          type: 'GET',
          url: 'http://kid37.cc.gatech.edu:8080/gpower/getAnswerNum.py?questionID=' + element.id,
          success: function(data) {
            reply_number.text("Reply:" + jQuery.parseHTML(data)[0].data)
          }
        });

        var by_user = $("<span>by </span>").append(
          $("<a href=\"mailto:"+element.contact+"\"></a>").text(element.userName)
        );
        var info = $("<div></div>").addClass("large-2 small-4 column pad")
          .append($("<span></span>").append(reply_number))
          .append(by_user);

        var line = $("<div class=\"large-12 forum-topic\">")
          .append(icon)
          .append(topic)
          .append(text1)
          .append(text2)
          .append(info);
        $("#question_list").append(line);
      });
    }
  });

});