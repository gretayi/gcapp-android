'use strict';

(function(){
  var commentsApp = angular.module('commentsApp', []);
  
  commentsApp.controller('CommentsController', function($scope, $filter){
    // Current comment.
    this.comment = {};

    // Array where comments will be.
    this.comments = [];

    // Fires when form is submited.
    this.addComment = function() {
      // Fixed img.
      this.comment.avatarSrc = 'http://sexar.pagelab.io/comments-angularjs/imgs/thumb-anonymous.jpg';

      // Add current date to the comment.
      this.comment.date = Date.now();

      var question_id = $.url().param("question_id");
      var url = 'http://kid37.cc.gatech.edu:8080/gpower/addAnswer.py?'
      + 'title=test&content=' + this.comment.text
      + '&questionID=' + question_id
      + '&isShow=true&timestamp=' + $filter('date')(this.comment.date, 'y/MM/dd hh:mm:ss')
      + '&username=' + this.comment.author
      + '&contact=' + this.comment.email;
      console.log(url);

      $.ajax({
        type: 'GET', 
        url: encodeURI(url),
        success: function(data) {
          console.log("success");
        }
      });
      $('.comment-form').hide();
      $('.reply-button').show();

      this.comments.push( this.comment );
      this.comment = {};

      // Reset clases of the form after submit.
      $scope.form.$setPristine();

    }

    // Fires when the comment change the anonymous state.
    this.anonymousChanged = function(){
      if(this.comment.anonymous)
        this.comment.author = "";
    }

  });

  var question_id = $.url().param("question_id");
  $.ajax({
    type: 'GET', 
    url: 'http://kid37.cc.gatech.edu:8080/gpower/getAnswerList.py?questionID='+question_id, 
    success: function(data) {
      var content = jQuery.parseHTML(data)[0].data;
      data = jQuery.parseJSON(content);
      $.each(data, function(index, element) {
        var comment = $("<div class=\"comment\"></div>");
        var comment_text = $("<div class=\"comment-text\"></div>").text(element.title);
        var comment_footer = $("<div class=\"comment-footer\"><div class=\"comment-info\"></div></div>")
          .append(
            $("<span class=\"comment-author\"></span>").append(
              $("<a></a>").attr("href", "mailto:"+element.contact).text(element.username)
            )
          ).append(
            $("<span class=\"comment-date\">").text(element.timestamp)
          );
        comment.append(
          $("<div class=\"comment-box\"></div>").append(comment_text).append(comment_footer)
        );
        $("#comment_list").append(comment);
      });
    }
  });

  $.ajax({
    type: 'GET', 
    url: 'http://kid37.cc.gatech.edu:8080/gpower/getQuestionById.py?questionID='+question_id, 
    success: function(data) {
      var content = jQuery.parseHTML(data)[0].data;
      data = jQuery.parseJSON(content);
      $.each(data, function(index, element) {
        $("#title_text").text("["+element.category+"]"+element.title).append("<br>");
        $("#time_text").text("\n"+element.timestamp).append("<br>");
        $("#detail_text").text("\n"+element.content);
      });
    }
  });

  $('.reply-button').click(function() {
    $('.comment-form').show();
    $('.reply-button').hide();
  });

})();