  
  function statusChangeCallback(response) {
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      $("#fb-login").hide();
      $("#fb-logout").show();
    } else {
      $("#fb-login").show();
      $("#fb-logout").hide();
    }
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '719505048149623',
      xfbml      : true,
      version    : 'v2.5'
    });
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  function login() {
    FB.login(function(response) {
      statusChangeCallback(response);
    }, {scope: 'publish_actions'});            
  }

  function logout() {
    FB.logout(function(response) {
      statusChangeCallback(response);
    });
  }  
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });   