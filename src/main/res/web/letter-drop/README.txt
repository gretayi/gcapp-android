A Pen created at CodePen.io. You can find this one at http://codepen.io/KryptoniteDove/pen/gipuw.

 Experimenting with more text effects using CSS animations and jQuery to keep the markup down. 

The delay is added via drop.styles() on the fly, you could I guess make all the CSS required on the fly using the same method but I've kept these separate for now. The animation effect, look and feel are customised via the CSS the way they should be. The JS plugin is a helper for all the repetition needed to create the effects on each letter.

Version 0.0.8 out now! Fork me on GitHub
https://github.com/KryptoniteDove/letter-drop